# OMF ROMs Extension
# 2024/03/02

[ `sed '/^id=/{s|id=||;q}' $MODPROP` = ohmyfont ] || return
ui_print "+ ROMs Extension"

# OOS10
[ -f $ORISYSETC/fonts_slate.xml ] && {
    cp $SYSXML $SYSETC/fonts_slate.xml
    OOS=true; ver slatexml; return
}

# OOS11
[ -f $ORISYSETC/fonts_base.xml ] && {
    cp $SYSXML $SYSETC/fonts_base.xml
    OOS11=true; ver basexml; return
}

# COS11/OOS12
[ -f $ORISYSEXTETC/fonts_base.xml ] && {
    falias sys-sans-en
    cp $SYSXML $SYSEXTETC/fonts_base.xml
    COS=true; ver xbasexml; return
}

$SANS || return

# MIUI
grep -q MIUI $ORISYSXML && {
    ver miui
    [ $API -eq 29 ] && return
    MIUI=`sed -n "/$FA.*\"miui\"/,$FAE{/400.*$N/{s|.*>||;p}}" $SYSXML`

    # MIUI A14
    case `getprop ro.build.version.incremental` in *U*XM)
        xml "/<$FA>/,${FAE}{d;q}"
        return;;
    esac

    # Lock all axes but wght
    [ -f $ORISYSFONT/$MIUI ] && [ $API -ge 31 ] && {
        totf || return
        ui_print '  Special treatments for MIUI (~60s)...'
        fonttools varLib.instancer -q -o $SYSFONT/$MIUI $TMPDIR/$SS \
            $(echo $(eval echo $(up $`ab $SS`r)) | \
            sed 's|\([[:alpha:]]\) \([[:digit:]]\)|\1=\2|g' | \
            sed 's|wght=[0-9.]*||') || abort
        return
    }

    # MIUI v13
    case `getprop ro.build.version.incremental` in V13*XM)
        #afdko || return
        xml "${SAF}H;/und-Yezi/,$FAE{${FAE}G}"
        xml ":a;N;\$!ba;s|name=\"$SA\"||2"
        return;;
    esac

    [ -f $ORISYSFONT/$MIUI ] && ln -s $X $SYSFONT/$MIUI
    [ -f $ORISYSFONT/RobotoVF$X ] && ln -s $X $SYSFONT/RobotoVF$X

    return
}

# Samsumg
grep -q Samsung $ORISYSXML && {
    fontab sec-roboto-light $SS r
    fontab sec-roboto-light $SS b M
    fontab sec-roboto-condensed $SS r
    fontab sec-roboto-condensed $SS b
    fontab sec-roboto-condensed-light $SS r L
    falias sec-no-flip
    falias sec
    falias roboto-num3L
    falias roboto-num3R
    SAM=true; ver sam; return
}
