# Oh My Font Installation Script
# by nongthaihoang @ GitLab

# debugging mode
set -xv

# Magisk and TWRP support
[ -d ${ORIDIR:=`magisk --path`/".magisk/mirror"}/system ] || \
      ORIDIR=

# Original paths
[ -d ${ORIPRD:=$ORIDIR/product} ] || \
      ORIPRD=$ORIDIR/system/product
[ -d ${ORISYSEXT:=$ORIDIR/system_ext} ] || \
   ORISYSEXT=$ORIDIR/system/system_ext

      ORISYS=$ORIDIR/system
  ORIPRDFONT=$ORIPRD/fonts
   ORIPRDETC=$ORIPRD/etc
   ORIPRDXML=$ORIPRDETC/fonts_customization.xml
  ORISYSFONT=$ORISYS/fonts
   ORISYSETC=$ORISYS/etc
ORISYSEXTETC=$ORISYSEXT/etc
   ORISYSXML=$ORISYSETC/fonts.xml

# Modules paths
        MODS=/data/adb/modules 
     ModPath=$MODS/$MODID
         SYS=$MODPATH/system
         PRD=$SYS/product
     PRDFONT=$PRD/fonts
      PRDETC=$PRD/etc
      PRDXML=$PRDETC/fonts_customization.xml
     SYSFONT=$SYS/fonts
      SYSETC=$SYS/etc
      SYSEXT=$SYS/system_ext
   SYSEXTETC=$SYSEXT/etc
      SYSXML=$SYSETC/fonts.xml
     MODPROP=$MODPATH/module.prop
       FONTS=$MODPATH/fonts
       TOOLS=$MODPATH/tools

        SERV=$MODPATH/service.sh
        POST=$MODPATH/post-fs-data.sh
        UNIN=$MODPATH/uninstall.sh

      OMFDIR=/sdcard/OhMyFont
      OMFVER=`grep ^versionCode= $MODPROP | sed 's|.*=||'`
       UCONF=$OMFDIR/config.cfg
      
# abbr. vars
     SysFont=/system/fonts
      SysXml=/system/etc/fonts.xml
        Null=/dev/null

# create module paths
mkdir -p $PRDFONT $PRDETC $SYSFONT $SYSETC $SYSEXTETC $FONTS $TOOLS $OMFDIR

# extract data
SH=$MODPATH/customize.sh
tail -n +$((`grep -an ^PAYLOAD:$ $SH | cut -d : -f 1`+1)) $SH | tar xJf - -C $MODPATH || abort

# bspatch tool - deprecated
#patch() { $TOOLS/bspatch $1 $1 $2; }

# placebo for afdko - print error if not installed
afdko() {
    [ $1 ] && ui_print '! The AFDKO extension is required!'
    false
}

# append text to the end of version string
ver() { sed -i "/^version=/s|$|-$1|" $MODPROP; }

# shortcut for sed fontxml
xml() {
    [ ${XML:=$SYSXML} ]
    case $XML_LIST in
        *$XML*) ;;
            # remove comments
        *)  sed -i '/^[[:blank:]]*<!--.*-->/d;/<!--/,/-->/d' $XML
            # change single quote to double quotes
            sed -i "s|'|\"|g" $XML
            # cut one line <font> tag to new lines
            sed -i "/<$F .*>/s|>|\n&|" $XML
            # merge multiple lines <font> tag into one line
            sed -i "/[[:blank:]]<$F /{:a;N;/>/!ba;s|\n||g}" $XML
            # cut <\font> tag to new line
            sed -i "/<$F.*$FE/s|$FE|\n&|" $XML
            # merge 2 lines <font> tag to one line
            sed -i "/<$F .*>$/{N;s|\n||}" $XML
            # join <\font> to <font> line if any
            sed -i "/<$F /{N;s|\n$FE|$FE|}" $XML
            # water mark
            sed -i "2i<!-- OMF v$OMFVER -->\n" $XML
            # save the font xml paths to xml list
            XML_LIST="$XML $XML_LIST" ;;
    esac
    sed -i "$@" $XML
}

# lowercase to uppercase
up() { echo $@ | tr [:lower:] [:upper:]; }

# Exucute extension scripts, 3 stages:
# script names starts in 0: run before anything else
# script names starts in 1-8: run before rom()
# script names starts in 9: run after rom()
src() {
    local l=`find $OMFDIR -maxdepth 1 -type f -name '*.sh' -exec basename {} \; | sort`
    if   [ "$1" = 0 ]; then l=`echo "$l" | grep '^0'`
    elif [ "$1" = 9 ]; then l=`echo "$l" | grep '^9'`
    else                    l=`echo "$l" | grep '^[^09]'`; fi
    local i
    for i in $l; do ui_print "+ Source $i"
        . $OMFDIR/$i
    done
}

# custom services support in $OMFDIR
svc() {
    $BOOTMODE || return
    local omfserv=$OMFDIR/service.d/*.sh
    local omfpost=$OMFDIR/post-fs-data.d/*.sh
    local omfunin=$OMFDIR/uninstall.d/*.sh

    # check for any custom service scripts
    ls $omfserv &>$Null || \
    ls $omfpost &>$Null || \
    ls $omfunin &>$Null && {
        ui_print '+ Services'

        # service.d
        ls $omfserv &>$Null && {
            echo 'MODDIR=${0%/*}' >> $SERV
            for i in $omfserv; do
                cp $i $MODPATH
                i=`basename $i`
                chmod +x $MODPATH/$i
                echo "\$MODDIR/$i &" >> $SERV
                ui_print "  $i"
            done
        }

        # post-fs-data.d
        ls $omfpost &>$Null && {
            echo 'MODDIR=${0%/*}' >> $POST
            for i in $omfpost; do
                cp $i $MODPATH
                i=`basename $i`
                chmod +x $MODPATH/$i
                echo "\$MODDIR/$i" >> $POST
                ui_print "  $i"
            done
        }

        # uninstall.d
        ls $omfunin &>$Null && {
            echo 'MODDIR=${0%/*}' >> $UNIN
            for i in $omfunin; do
                cp $i $MODPATH
                i=`basename $i`
                chmod +x $MODPATH/$i
                echo "\$MODDIR/$i &" >> $UNIN
                ui_print "  $i"
            done
        }
    }
}

# cp files from $FONT to $SYSFONT, do not overwrite
cpf() {
    [ $# -eq 0 ] && return 1
    local i
    for i in $@; do
        false | cp -i $FONTS/$i ${CPF:=$SYSFONT} &>$Null
    done
}

# Rom preparation. E.g. Pixels need to be detected early for advanced functions
romprep() {
    src 0
    [ -f $ORIPRDFONT/$GSR ] && grep -q $Gs $ORIPRDXML && \
        PXL=true
}

# ROMs modification logic
rom() {
    # source extensions 1-8 (2nd stage)
    src
    # Some ROMs are detected as Pixel but they are fake ones.
    # The PXL=false option is to circumvent this situation
    local pxl=`valof PXL`
    [ $PXL ] && [ "$pxl" = false ] && PXL=

    # inject GSVF into fontxml
    $SANS && [ $GS = false -o $GS = $SE ] && {
        local fa=$Gs.* xml=$FONTS/gsvf.xml m=verdana
        xml "/$m/r $xml"
        # Disable gms font service. Thanks to @MrCarb0n
        $BOOTMODE && (
            ${rmf:="rm /data/user/0/com.google.android.gms/files/fonts/opentype/Google_Sans*"}
            gms=com.google.android.gms/com.google.android.gms.fonts
            gms1=$gms.provider.FontsProvider
            gms2=$gms.update.UpdateSchedulerService
            echo "( until pm disable $gms1; do sleep 30; done; pm disable $gms2; $rmf ) &" >> $SERV
            echo "( until pm enable $gms1; do sleep 5; done; pm enable $gms2 ) &" >> $UNIN
        )
        [ $PXL ] || {
            local up=$SS it=$SSI
            fontinst
        }
    }

    [ $PXL ] && pxl
    # source extension - 9 (3rd stage)
    src 9
}

pxl() {
    ver pxl

    # GS=true do nothing
    [ $GS = true ] && return

    cp $ORIPRDXML $PRDXML
    local gs=$GS XML=$PRDXML fa=$Gs.* axis_del up it i

    # remove GS from PRDXML
    $SANS && [ $GS = false -o $GS = $SE ] && {
        # hide GSC from being deleted
        xml "s|$Gs-clock|GSC|;s|$Gs-flex|GSF|"
        xml "/$FA.*$fa/,${FAE}d"
        xml "s|GSC|$Gs-clock|;s|GSF|$Gs-flex|"
        XML=
    } || return

    # GS=serif
    [ $gs = $SE ] && {
        # install NY for GS then set family to GSText
        up=$NY it=$NYI
        fontinst
        gs=false fa=$Gs-text.*

        # A12+, set min opsz 20 for NY (headlines need big opsz)
        [ $API -ge 31 ] && {
            axis_del=false
            echo $SR | grep -q 'opsz 1[2-9]' && \
                font $Gs $up r opsz 20
            axis_del=
        }

        # if sans = serif then quit here
        [ $SANS = $SE ] && return
    }


    # GS=false
    [ $gs = false ] || return
    up=$SF it=$SFI
    fontinst

    # OPSZ tweaks for GS big headlines
    # family = GSText means GS = serif
    # and user does not set 1 opsz for all
    [ $fa != $Gs-text.* ] && $OPSZs && {
        local axis_del=false opsz a

        # A12+, big headlines need big opsz
        # Regular
        [ $API -ge 31 ] && opsz=`echo $OPSZ 3 + p | dc` || \
            opsz=`echo $OPSZ 1 + p | dc`
        echo `valof UR` | grep -Eq "$Re|OPSZ" && {
            a=opsz; echo $UR | grep -q SPAC && a=SPAC
            font $Gs $up r $a $opsz
        }
        
        # Medium
        opsz=`echo $OPSZ 1 + p | dc`
        echo `valof UM` | grep -Eq "$Me|OPSZ" && {
            a=opsz; echo $UM | grep -q SPAC && a=SPAC
            for i in $Gs $Gs-medium; do
                font $i $up m $a $opsz
            done
        }
        axis_del=
    }

    # replace GS font file
    [ $GS = false ] && ln -s $SysFont/$RR $PRDFONT/$GSR

    # A12+, minimum opsz for GS is 20 to 
    # prevent the temperature degree text in lock screen get ellipses
    [ $API -ge 31 ] && [ ${OPSZ%.*} -lt 20 ] && {
        font $Gs-text $up r $UR
        xml "/$FA.*$fa/,$FAE{/\"1[789]/s|1[789][.0-9]*|20|}"
    }
}

vars() {
    # xml
    FA=family FAE="/\/$FA/" F=font FE="<\/$F>"
    W=weight S=style I=italic N=normal ID=index=
    FF=fallbackFor FW='t el l r m sb b eb bl'
    readonly FA FAE F FE W S I N ID FF FW

    # families
    SE=serif SA=sans-$SE SAQ="/\"$SA\">/" SAF="$SAQ,$FAE"
    SC=$SA-condensed MO=monospace SO=$SE-$MO
    readonly SE SA SAQ SAF SC MO SO

    # styles
    Bl=Black Bo=Bold EBo=Extra$Bo SBo=Semi$Bo Me=Medium
    Th=Thin Li=Light ELi=Extra$Li Re=Regular It=Italic
    Cn=Condensed- St=Static
    readonly Bl Bo EBo SBo Me Th Li ELi Re It Cn St

    # font ext.
    X=.ttf Y=.otf Z=.ttc XY=.[ot]tf XYZ=.[ot]t[tc]
    readonly X Y Z XY XYZ

    # font names
    SF=SFUI$X SFI=SFUI$It$X SFR=SFUIRounded$X
    SFM=SFUIMono$X SFMI=SFUIMono$It$X PS=PowerlineSymbols$Y
    NY=NewYork$X NYI=NewYork$It$X
    CP=CourierPrime$Z
    readonly SF SFI SFR SFM SFMI NY NYI CP

    # subtitutes font families
    SS=$SF SSI=$SFI SSS=$SFS SER=$NY SERI=$NYI
    MS=$SFM MSI=$SFMI SRM=$CP SRMI=$CP FB=fallback

    # default android font names
    Ro=Roboto Ns=NotoSerif
    Ds=DroidSans$X Dm=DroidSansMono Cm=CutiveMono
    RR=$Ro-$Re$X RS=$Ro$St-$Re$X
    GSR=GoogleSans-$Re$X GSI=GoogleSans-$It$X
    Gs=google-sans GSC=GoogleSansClock-$Re$X
    readonly Ro Ns Ds Dm Cm RR RS GSR GSI Gs GSC
}

# prepare font xml for installation
prep() {
    #[ -f $ORISYSXML ] || abort
    vars; romprep

    # updating the module but can't access the original fontxml, ABORT!
    [ -d $ModPath/system ] && ! [ $ORIDIR ] && \
        abort "! Please remove the module first. Then try again!"

    # compatible mode
    COMP=`valof COMP`
    ${COMP:=false} && {
        ui_print "! Compatible Mode!"
        ver '<!>'
        false | cp -i $SysXml $SYSXML &>$Null
        find $MODS* -not -path "*/$MODID/*" -type f -name "fonts*xml" -delete
        return
    }

    # remove fontxml from other installed modules to prevent conflicts 
    [ "`find $MODS* -not -path "*/$MODID/*" -type f -name "fonts*xml" -print -delete`" ] && {
        ui_print "! Warning. Conflicted Module Detected!"
        ver '<!>'
        false | cp -i $ORISYSXML $SYSXML &>$Null
        REBOOT=false
        return
    }

    false | cp -i $ORISYSXML $SYSXML &>$Null
}

font() {
    local fa=${1:?} f=${2:?} w=${3:-r} s=$N r i

    # SF Rounded filter
    [ $f = $SF ] && case $* in *SPAC*) f=$SFR ;; esac

    # check for ttc
    case $f in *c) i=$ID          ;; esac
    # serif
    case $w in *s) r=$SE w=${w%?} ;; esac
    # italics
    case $w in *i) s=$I  w=${w%?} ;; esac
    # convert weight names to numbers
    case $w in
        t ) w=1 ;; el) w=2 ;; l ) w=3 ;;
        r ) w=4 ;; m ) w=5 ;; sb) w=6 ;;
        b ) w=7 ;; eb) w=8 ;; bl) w=9 ;;
    esac
    fa="/$FA.*\"$fa\"/,$FAE" s="${w}00.*$s"
    # italics
    [ $i ] && s="$s.*$i\"[0-9]*"
    # serif; postScriptname
    [ $r ] && s="$s.*\"$r"; s="$s\"[[:blank:]]*[p>]"

    # cut </font> tag to new line
    xml "$fa{/$s/s|$FE|\n&|}"
    # if axis_del is true then remove all <axis> tags
    $axis_del && xml "$fa{/$s/,/$FE/{/$F/!d}}"
    # Replace font name
    xml "$fa{/$s/s|>.*$|>$f|}"
    # check index if ttc
    [ $4 ] && [ $i ] && {
        xml "$fa{/$s/s|$i\".*\"|$i\"$4\"|}"
        return
    }

    # remove all but axes
    shift 3; [ $# -eq 0 -o $? -ne 0 ] && {
        xml "$fa{/$s/{N;s|\n$FE|$FE|}}"
        return
    }
    # axes
    f="$s.*$f" s="/$f/,/$FE/"; local t v a
    while [ $2 ]; do
        t="tag=\"$1\"" v="stylevalue=\"$2\""
        a="<axis $t $v/>"; shift 2
        xml "$fa{$s{/$t/d};/$f/s|$|\n$a|}"
    done
}

# abbreviations font names for font styles configs
# $1: font, $2: family, $3: style
ab() {
    local n=z
    # check ups var for manually font style prefix
    [ $ups ] && n=$ups || \
    case $1 in
        $SF |$SFR ) n=u ;;
        $SFI      ) n=i ;;
        $NY |$NYI ) n=s ;;
        $SFM|$SFMI) n=m ;;
        $GSR|$GSI ) n=g ;;
    esac
    case "$3" in *i)
        case $n in
            # if ups, check its var
            $ups) [ $its ] && n=$its ;;
        esac
    esac
    # condensed
    [ "$2" = $SC -a $n = u ] && n=c
    echo $n
}

# shortcut for font() with auto-axes recognition from ab()
fontab() {
    local w=${4:-$3}; case $w in *i) w=${w%?} ;; esac
    eval font $1 $2 $3 \$$(up `ab $2 $1 $3`$w)
}

# shortcut for font() without arguments, auto-read values from input vars
fontinst() {
    # VFs
    case $up in *.*)
        [ $up ] && cpf $up
        [ $it ] && cpf $it
        local i
        for i in ${@:-$FW}; do
            [ $up ] && {
                fontab $fa $up $i
                $condensed && [ $fa = $SA ] && fontab $SC $up $i
            }
            [ $it ] && {
                fontab $fa $it ${i}i
                $condensed && [ $fa = $SA ] && fontab $SC $it ${i}i
            }
        done
        return ;;
    esac

    # Static fonts
    set bli $Bl$It bl $Bl ebi $EBo$It eb $EBo bi $Bo$It b $Bo \
        sbi $SBo$It sb $SBo mi $Me$It m $Me ri $It r $Re \
        li $Li$It l $Li eli $ELi$It el $ELi ti $Th$It t $Th
    while [ $2 ]; do
        cpf $up$2$X && font $fa $up$2$X $1 && \
            $condensed && [ $fa = $SA ] && {
                cpf ${up%?}$Cn$2$X && font $SC ${up%?}$Cn$2$X $1 || \
                    { $FULL && font $SC $up$2$X $1; }
            }
        shift 2
    done
}

# makes font styles - thin to black in fontxml
mksty() {
    case $1 in [a-z]*) local fa=$1; shift ;; esac
    local max=${1:-9} min=${2:-1} dw=${3:-1} id=$4 di=${5:-1} fb

    [ $fa ] || local fa=$SA
    local fae="/$FA.*\"$fa\"/,$FAE"
    # if font_del then delete all existing <font> tag in a <family> tag
    $font_del && xml "$fae{/$FA/!d}"

    local i=$max j=0 s
    # index ttc
    [ $id ] && j=$id && id=" $ID\"$j\""
    # fallback for ...
    [ $fallback ] && fb=" $FF=\"$fallback\""
    until [ $i -lt $min ]; do
        for s in $I $N; do
            eval \$$s || continue
            xml "$fae{/$fa/s|$|\n<$F $W=\"${i}00\" $S=\"$s\"$id$fb>$FE|}"
            [ $j -gt 0 ] && j=$(($j-$di)) && id=" $ID\"$j\""
        done
        [ $i -gt 4 -a $(($i-$dw)) -lt 4 ] && \
            i=4 min=4 || i=$(($i-$dw))
    done

    # remove weights
    for i in $wght_del; do xml "$fae{/${i}00/d}"; done
}

# shortcut for mksty(), auto-detect font styles from config (VF) or font files (static)
mkstya() {
    # VFs
    case $up in *.*)
        local wght_del i j=1 k=false
        [ $it ] || local italic=false

        for i in $FW; do
            # check and delete empty font weights
            eval [ \"\$$(up `ab $up`$i)\" ] && k=true || \
                wght_del="$wght_del $j"
            j=$((j+1))
        done
        # if all font styles are empty, make only Regular
        $k || {
            wght_del=
            mksty 4 4
            $condensed && [ $fa = $SA ] && mksty $SC 4 4
            return
        }

        mksty
        $condensed && [ $fa = $SA ] && mksty $SC
        return ;;
    esac

    # Static fonts
    local i=9 italic font_del
    set $Bl$It $Bl $EBo$It $EBo $Bo$It $Bo \
        $SBo$It $SBo $Me$It $Me $It $Re \
        $Li$It $Li $ELi$It $ELi $Th$It $Th
    while [ $2 ]; do
        italic=
        # make font styles based on actual font files
        [ -f $FONTS/$up$1$X ] || italic=false
        [ -f $FONTS/$up$2$X ] && {
            mksty $i $i
            $condensed && [ $fa = $SA ] && mksty $SC $i $i
            font_del=false
        }
        i=$((i-1)); shift 2
    done
}

# make fallback font, i.e., make Roboto a fallback font to avoid missing glyphs
fallback() {
    local faq fae fb
    [ $1 ] && local fa=$1; [ $fa ] || local fa=$SA
    faq="\"$fa\"" fae="/$FA.*$faq/,$FAE"
    # add "fallbackFor" to <font> tags
    [ $fa = $SA ] || fb="/<$F/s|>| $FF=$faq>|;"
    # make new family instead of fallback
    [ $name ] && name=name=\"$name\" fb=

    # remove/replace family name from the 2nd occurrence
    xml "$fae{${fb}H;2,$FAE{${FAE}G}}"
    xml ":a;N;\$!ba;s|name=$faq|$name|2"
    # if fallback, revert changes on the original family
    [ "$fb" ] && xml "$fae{s| $FF=$faq||
        s| postScriptName=\"[^ ]*\"||}"
}

# check a family if it is alread fallback
# do not make a family fallback more than once
fba() {
    # List of fallback fonts.
    [ "${FBL:=`sed -n "/<$FA *>/,$FAE{/400.*$N/p}" $SYSXML`}" ]
    # Roboto
    if   [ "$fa" = $SA ]; then echo $FBL | grep -q $Ro || fallback
    # NotoSerif
    elif [ "$fa" = $SE ]; then echo $FBL | grep -q $Ns || fallback
    # DroidSansMono
    elif [ "$fa" = $MO ]; then echo $FBL | grep -q $Dm || fallback
    # Cutive Mono
    elif [ "$fa" = $SO ]; then echo $FBL | grep -q $Cm || fallback; fi
    # PowerlineSymbols
    if   [ $up = $PS ]; then echo $FBL | grep -q "$ID\"7\">$RS" || return 1; fi
}

# Contextual Alternates
calt() {
    afdko || return
    
    # extract GSUB table
    local f=${1:?}; shift
    ttx -s -t GSUB -f $f &>$Null
    
    # insert lookup index values
    local i t=${f%$X}.G_S_U_B_.ttx \
        f=Feature v=value= id=$ID l=LookupListIndex
    for i in $@; do
        sed -i "/<${f}Tag $v\"calt\"\/>/,/<\/$f>/{
        /<\/$f>/s|^|<$l $id\"9\" $v\"$i\"/>\n|}" $t
    done
}

# font features
otl() {
    [ "$OTL" ] && totf || return

    # List of font features lookup index values of SF
    [ "$1" = 0 ] || ui_print '  OpenType Layout'
    local font ttx k otl a i j l f s n z h t
    for k in $SF $SFI $SFR; do
        font=$TMPDIR/$k ttx=${font%$X}.G_S_U_B_.ttx otl=

        # SF
        if   [ $k = $SF  ]; then
            a=41 i=45 j=46 l=43 f=49 s=50 n=51 z=48
            t=28 h="$i $j $l $f $s $n $z"
        # SF Italic
        elif [ $k = $SFI ]; then
            a=34 i=32 j=   l=33 f=31 s=29 n=30 z=35
            t=14 h=27
        # SF Rounded
        elif [ $k = $SFR ]; then
            a=35 i=33 j=   l=34 f=32 s=30 n=31 z=36
            t=14 h=28
        fi

        # OTL table
        case $OTL in *a*) otl="$otl $a" ;; esac
        case $OTL in *i*) otl="$otl $i" ;; esac
        case $OTL in *j*) otl="$otl $j" ;; esac
        case $OTL in *l*) otl="$otl $l" ;; esac
        case $OTL in *f*) otl="$otl $f" ;; esac
        case $OTL in *s*) otl="$otl $s" ;; esac
        case $OTL in *n*) otl="$otl $n" ;; esac
        case $OTL in *z*) otl="$otl $z" ;; esac
        case $OTL in *h*) otl="$h     " ;; esac
        case $OTL in *t*) otl="$otl $t" ;; esac

        # import modified GSUB table
        [ "$otl" ] && calt $font $otl && \
            $TOOLS/pyftimport $font $ttx || break
    done
    OTL=
}

sfui() {
    local up=$SF it=$SFI fa=${1:-$SA}
    [ $fa = $SA ] && local condensed=false

    fba; mkstya; fontinst
    # SF doesn't have condensed italics
    # Android will generate them automatically
    [ $fa = $SA ] && { fa=$SC it=; mkstya; fontinst; }
    # SF Rounded
    grep -q $SFR $SYSXML && ui_print '  Rounded' && ROUNDED=true
    [ ${ROUNDED:=false} ]

    # Font features
    otl
}

newyork() {
    local up=$NY it=$NYI fa=${1:-$SE}
    fba; mkstya; fontinst
}

# powerline symbols are well-known among terminal users
powerline() {
    local up=$PS it fa=${1:-$MO} italic=false
    # This is a fallback font, check if it's already fallback or make it fallback
    fba && return
    mksty 4 4; fontinst r; fallback
}

sfmono() {
    local up=$SFM it=$SFMI fa=${1:-$MO}
    # Powerline is part of monospace
    powerline $fa; mkstya; fontinst
}

courier() {
    local up=$CP it=$CP fa=${1:-$SO}
    # ttc index font
    fba; mksty 7 4 3 3; fontinst r b
}

# line height
line() {
    [ "$LINE" != 1.0 ] && totf || return

    # change font ascender and descender proportionally instead of using Roboto's
    # This is better in term of keeping font quality
    ui_print '+ Line spacing'
    # apply for all font families to achieve an unique looks
    local i
    for i in $SF $SFI $SFR $SSS $NY $NYI $SFM $SFMI \
        ${CP%$Z}-$Re$X ${CP%$Z}-$It$X ${CP%$Z}-$Bo$X ${CP%$Z}-$Bo$It$X
    do $TOOLS/pyftline $TMPDIR/$i $LINE || abort; done
}

# extract ttc to ttf
totf() {
    afdko 1 || return 1
    [ "$TOTF" ] && return

    # extract ttc and rename extracted font files to match their true names
    # Roboto-Regular
    cp $FONTS/$RR $TMPDIR/${RR%?}c
    otc2otf $TMPDIR/${RR%?}c
    set .${SF%$X}-$Re $SF .${NY%$X}-$Re $NY .${SFM%$X}-$Li $SFM \
        ${SFR%$X}-$Re $SFR
    while [ $2 ]; do mv $TMPDIR/$1$X $TMPDIR/$2; shift 2; done

    # RobotoStatic
    cp $FONTS/$RS $TMPDIR/${RS%?}c
    otc2otf $TMPDIR/${RS%?}c
    set .${SF%$X}-$Re$It $SFI .${NY%$X}-$Re$It $NYI .${SFM%$X}-$Li$It $SFMI
    while [ $2 ]; do mv $TMPDIR/$1$X $TMPDIR/$2; shift 2; done

    # make it true to avoid extract them more than once
    TOTF=true
}

# merge otf to otc
totc() {
    [ "$TOTF" = true ] || return 1

    # Roboto-Regular
    otf2otc -o $FONTS/$RR \
        $TMPDIR/$SF $ORISYSFONT/$RR $TMPDIR/$NY $TMPDIR/$SFM \
        $TMPDIR/${CP%$Z}-$Re$X $TMPDIR/${CP%$Z}-$Bo$X \
        $TMPDIR/$SFR &>$Null

    # RobotoStatic
    otf2otc -o $FONTS/$RS \
        $TMPDIR/$SFI $TMPDIR/$RS $TMPDIR/$NYI $TMPDIR/$SFMI \
        $TMPDIR/${CP%$Z}-$It$X $TMPDIR/${CP%$Z}-$Bo$It$X \
        $TMPDIR/$PS &>$Null
}

# make a family alias to another
falias() {
    # alias to sans-serif by default
    local fa faq fae to=to=\"${2:-$SA}\"
    fa=${1:?} faq="/\"$fa\">/" fae="$faq,$FAE"
    # insert <alias> tag
    xml "$faq i<alias name=\"$fa\" $to />"
    # delete old family, redirect others to new one
    xml "${fae}d"; xml "s|to=\"$fa\"|$to|"
}

# replace GS Clock font in the systemui apk (A12)
# replace GS Clock font (A13)
lsc(){
    $SANS && [ $GS != true -a $LSC != false -a $API -ge 31 ] && totf || return
    ui_print '+ Lock Screen Clock'
    
    # A12
    [ $API -lt 33 ] && {
        local privapp=${ORISYSEXT//$ORIDIR\//}/priv-app
        local app=$privapp/SystemUIGoogle
        [ -d $ORIDIR/${app%G*} ] && app=${app%G*}
        local apk=$app/SystemUIGoogle.apk
        [ -f $ORIDIR/${apk%G*}.apk ] && apk=${apk%G*}.apk
        local modprivapp=$SYS/${privapp//system\//}
        local modapp=$SYS/${app//system\//}
        local modapk=$SYS/${apk//system\//}
        local fdir=$modapp/res/font
        local font=$fdir/google_sans_clock$X

        mkdir -p $fdir
        cp $ORIDIR/$apk $modprivapp || abort
    }

    [ $font ] || local font=$PRDFONT/$GSC
    [ $LSC = def ] && cp $FONTS/SFUILSC$X $font || {
        local ss=$SS
        [ $ss = $SF ] && echo $UR | grep -q SPAC && ss=$SFR
        [ $GS = $SE ] && ss=$NY
        local lsc=$TMPDIR/$ss
        [ $LSC = cust ] && {
            [ -f $OMFDIR/lsc$XY ] && {
                lsc=$OMFDIR/lsc$XY
                ui_print '  Custom'
            } || abort '! Font not found!'
        }

        # enable tabular numbers and centered colon
        [ -z "${LSCOTL:=`valof LSCOTL`}" -a $LSC = true ] && \
            LSCOTL=tnum,ss03; [ ${LSCOTL:=tnum} ]
        pyftfeatfreeze -f $LSCOTL $lsc $TMPDIR/lsc$X &>$Null
        [ -f $TMPDIR/lsc$X ] || cp $lsc $TMPDIR/lsc$X

        # only keep numbers and colon
        pyftsubset $TMPDIR/lsc$X --unicodes=u30-3a \
            --passthrough-tables --output-file=$font

        # style
        [ -z "${LSCSTY:=`valof LSCSTY`}" ] && [ $GS = $SE ] && LSCSTY="opsz 128"
        [ "$LSCSTY" ] && fonttools varLib.instancer -q -o $font $font \
            `echo $LSCSTY | sed 's|\([[:alpha:]]\) \([[:digit:]]\)|\1=\2|g'`

        # fix padding
        [ ${LSCLINE:=`valof LSCLINE`} ] && $TOOLS/pyftline $font $LSCLINE
        $TOOLS/pyftlsc $font
    }

    # A13 done here
    [ $API -ge 33 ] && return

    # patch apk (A12)
    ( cd $modapp
    $TMBIN/zip -qr $modapp.apk *
    $TMBIN/zipalign -p -f 4 $modapp.apk $modapk ) || abort
    rm -r $modapp.apk $modapp/res
}

# fix VF default weight is not Regular, status bar padding
fontfix() {
    local i f=$@
    [ "$f" ] && afdko 1 || return
    for i in $f; do $TOOLS/fontfix $i; done
}

# font spoofing, the actual font installation starts here
fontspoof() {
    falias source-sans-pro
    $SANS || $SERF || $MONO || $SRMO || return
    line; totc; cpf $RR $RS; local id=" $ID"
    # original Robotos are at index 1
    xml "s|>$RR|$id\"1\"&|;s|>$RS|$id\"1\"&|"

    # convert font names to its corresponding indexes
    $SANS && ( xml        "s|$SF|$RR|
                          s|$SFI|$RS|
                         s|>$SFR|$id\"6\">$RR|" )
    $SERF && xml         "s|>$NY|$id\"2\">$RR|
                         s|>$NYI|$id\"2\">$RS|"
    $MONO && xml        "s|>$SFM|$id\"3\">$RR|
                        s|>$SFMI|$id\"3\">$RS|
                          s|>$PS|$id\"6\">$RS|"
    $SRMO && xml "s|$id\"0\">$CP|$id\"4\">$RR|
                  s|$id\"1\">$CP|$id\"4\">$RS|
                  s|$id\"2\">$CP|$id\"5\">$RR|
                  s|$id\"3\">$CP|$id\"5\">$RS|"

    # re-copy fontxml after new modifications
    if   [ $OOS   ]; then cp $SYSXML $SYSETC/fonts_slate.xml
    elif [ $OOS11 ]; then cp $SYSXML $SYSETC/fonts_base.xml
    elif [ $COS   ]; then cp $SYSXML $SYSEXTETC/fonts_base.xml
    elif [ $PXL   ]; then lsc
    fi
}

# read value from the config, strip duplicate spaces
# the 1st argument is the maxium number of values that a variable can have
valof() {
    sed -n "s|^$1[[:blank:]]*=[[:blank:]]*||p" $UCONF | \
        sed 's|[[:blank:]][[:blank:]]*| |g;s| $||' | \
        tail -${2:-1}
}

# convert a predifined instance to its preset
styof() {
    local s p
    # check if the config file exists. And value is not empty
    [ -f $UCONF ] && [ "${s:=`valof $1`}" ] || return

    # set optical sizes based on the global OPSZ
    # SF Italic does not have GRAD axis
    # fake GRAD by increase wght and opsz
    # opsz for SF Italic is OPSZ + 1
    local opsz=${opsz:-$OPSZ}
    p=$(sed -n "/^# $s$/{n;s|^# ||
        s|IOPSZ|`echo $opsz 1 + p | dc`|
        s|OPSZ|$opsz|
        p}" $UCONF | tail -1)

    # check for misconfiguration and delete the config file
    [ "$p" ] && echo $p || {
        echo $s | grep -Eq 'wdth|opsz|GRAD|wght|YAXS|SPAC' && \
        echo $s | sed "
            s|IOPSZ|`echo $opsz 1 + p | dc`|
            s|OPSZ|$opsz|" || \
            rm $UCONF
    }
}

# read font styles config for VF
getsty() {
    local i
    for i in `up $FW`; do
        eval ${ups:?}$i=\"`styof $ups$i`\"
        [ $its ] && eval $its$i=\"`styof $its$i`\"
    done
}

config() {
    local dconf dver uver
    # 3 hash signs is used for integrity check
    dconf=$MODPATH/config.cfg
    dver=`sed -n '/###/,$p' $dconf`
    uver=`sed -n '/###/,$p' $UCONF`
    [ "$uver" != "$dver" ] && {
        # backup old config and reset
        cp $UCONF $UCONF~; cp $dconf $UCONF
        ui_print '  Reset'
    }

    # global options
    SANS=`valof SANS` SERF=`valof SERF` MONO=`valof MONO` SRMO=`valof SRMO`
    GS=`valof GS`     OPSZ=`valof OPSZ` OTL=`valof OTL`
    LINE=`valof LINE` LSC=`valof LSC`

    # default values if empty
    [ ${SANS:=true} ]; [ ${SERF:=true} ]; [ ${MONO:=true} ]; [ ${SRMO:=true} ]
    [ ${GS:=true}   ]; [ ${LINE:=1.0}  ]; [ ${LSC:=false} ];

    # dynamic optical sizes
    # headlines/bold text need bigger opsz to look good
    # use suffix a in the OPSZ to bypass this
    case $OPSZ in *a) OPSZs=false OPSZ=${OPSZ%?} ;; esac
    local opsz i
    for i in $FW; do opsz=${OPSZ:?}
        $OPSZs && \
            case $i in
                t ) opsz=`echo $opsz 1 - p | dc` ;;
                el) opsz=`echo $opsz 1 - p | dc` ;;
                l ) opsz=`echo $opsz 0 - p | dc` ;;
                m ) opsz=`echo $opsz 0 + p | dc` ;;
                sb) opsz=`echo $opsz 1 + p | dc` ;;
                b ) opsz=`echo $opsz 1 + p | dc` ;;
                eb) opsz=`echo $opsz 2 + p | dc` ;;
                bl) opsz=`echo $opsz 2 + p | dc` ;;
            esac; i=`up $i`
        eval U$i=\"`styof U$i`\"
        eval I$i=\"`styof I$i`\"
        eval [ \"\${I$i:=\$U$i}\" ]
        eval C$i=\"`styof C$i`\"
        eval [ \"\${C$i:=\$U$i}\" ]
        eval M$i=\"`styof M$i`\"
        eval S$i=\"`styof S$i`\"
        # reset after a misconfiguration
        [ ! -f $UCONF ] && config && break
    done
}

# these functions only exist for compatibility reason
# when execute font families vars, e.g. $SANS && ...
sans_serif() { true; }
serif() { true; }
serif_monospace() { true; }
monospace() { true; }

install_font() {
    ui_print ' _____ _   _____     _____         _   '
    ui_print '|     | |_|     |_ _|   __|___ ___| |_ '
    ui_print '|  |  |   | | | | | |   __| . |   |  _|'
    ui_print '|_____|_|_|_|_|_|_  |__|  |___|_|_|_|  '
    ui_print '                |___|                  '

    ui_print '- Installing'
    ui_print '+ Prepare'
    prep
    ui_print '+ Configure'
    config

    # sans-serif: SF or NY
    $SANS && {
        ui_print '+ sans-serif'
        if [ $SANS = true ]; then
            ui_print '  San Francisco Pro'
            sfui
        elif [ $SANS = $SE ]; then
            ui_print '  New York'
            newyork $SA; SS=$NY SSI=$NYI
        fi
    }

    # serif: SF or NY
    $SERF && {
        ui_print '+ serif'
        if [ $SERF = true ]; then
            ui_print '  New York'
            newyork
        elif [ $SERF = sans_$SE ]; then
            ui_print '  San Francisco Pro'
            sfui $SE; SER=$SF SERI=$SFI
        fi
    }

    # monospace: SFMono or Courier
    $MONO && {
        ui_print '+ monospace'
        if [ $MONO = true ]; then
            ui_print '  San Francisco Mono'
            sfmono
        elif [ $MONO = serif_$MO ]; then
            ui_print '  Courier Prime'
            courier $MO; MS=$CP MSI=$CP
        fi
    }

    # serif-monospace: SFMono or Courier
    $SRMO && {
        ui_print '+ serif-monospace'
        if [ $SRMO = true ]; then
            ui_print '  Courier Prime'
            courier
        elif [ $SRMO = $MO ]; then
            ui_print '  San Francisco Mono'
            sfmono $SO; SRM=$SFM SRMI=$SFMI
        fi
    }

    ui_print '+ ROM'
    rom

    ui_print '- Finalizing'
    fontspoof
    svc
    finish
}

# remove unused files and folders, set permissions, unmount afdko
finish() {
    find $MODPATH/* -maxdepth 0 \
        ! -name 'system' \
        ! -name 'zygisk' \
        ! -name '*.rule' \
        ! -name '*.prop' \
        ! -name '*.sh' -exec rm -rf {} \;
    find $MODPATH/* -type d -delete &>$Null
    find $MODPATH/system -type f -exec chmod 644 {} \;
    find $MODPATH/system -type d -exec chmod 755 {} \;
    [ "$AFDKO" = true ] && { umount $TERMUX; rmdir -p $TERMUX; }
}

restart() {
    $BOOTMODE && ${REBOOT:-true} || return
    # always reboot on first install to make sure the config file is readable
    [ -d $ModPath/system ] || {
        ui_print '! Rebooting in 5s...'; sleep 5
        reboot
    }
    REBOOT=`valof REBOOT`; ${REBOOT:=false} || return 
    ui_print '! Rebooting in 5s...'; sleep 5 
    local old=`find $ModPath/system \( -type f -o -type l \) -exec basename {} \;`
    local new=`find $MODPATH/system \( -type f -o -type l \) -exec basename {} \;`
    [ "$old" = "$new" ] || reboot
    cp -r $MODPATH/system $ModPath
    setprop ctl.restart zygote || reboot
}

install_font

trap restart 0
return

PAYLOAD:
