# OH MY FONT!
**[Wiki](https://gitlab.com/nongthaihoang/oh_my_font/-/wikis/home) —
[Discussion](https://t.me/ohmyfont) —
[Download](https://gitlab.com/nongthaihoang/oh_my_font/-/raw/master/releases/OMF.zip) —
[Changelog](https://gitlab.com/nongthaihoang/oh_my_font/-/blob/master/CHANGELOG.md)**

## Descriptions
OMF is a Magisk module — A collection of typefaces — To improve Android Typography.

## Fonts

#### [**SF Pro**](https://nongthaihoang.gitlab.io/omf-demo)  
> This neutral, flexible, **sans-serif** typeface is the system font for iOS, iPad OS, macOS and tvOS. SF Pro features nine weights, variable optical sizes for optimal legibility, and includes a rounded variant. SF Pro supports over 150 languages across Latin, Greek, and Cyrillic scripts.

#### [**New York**](https://nongthaihoang.gitlab.io/omf-demo/newyork.html)  
> A companion to San Francisco, this **serif** typeface is based on essential aspects of historical type styles. New York features six weights, supports Latin, Greek and Cyrillic scripts, and features variable optical sizes allowing it to perform as a traditional reading face at small sizes and a graphic display face at larger sizes.

#### [**SF Mono**](https://nongthaihoang.gitlab.io/omf-demo/sfmono.html)  
> This **monospaced** variant of San Francisco enables alignment between rows and columns of text, and is used in coding environments like Xcode. SF Mono features six weights and supports Latin, Greek, and Cyrillic scripts.

#### [**Courier Prime**](https://quoteunquoteapps.com/courierprime/)  
> Courier is a **monospaced slab serif** typeface, Courier Prime is a variant of Courier. Courier Prime includes a true Italic style and renders a wider range of Unicode characters than most Courier variants, with some design changes and improvements aimed at greater legibility and beauty.

## Extensions
#### [**ROMs**](https://gitlab.com/nongthaihoang/oh_my_font/-/raw/master/extensions/90_rom.sh?inline=false)
- Required for ROMs/devices other than Google Pixel.

#### [**TWRP**](https://gitlab.com/nongthaihoang/oh_my_font/-/raw/master/extensions/twrp.zip) (non-root)
- Support TWRP recovery (un)installation.
- Credit [coreutils](https://github.com/Zackptg5/Cross-Compiled-Binaries-Android/tree/master/coreutils) @ Zackptg5

#### [**NotoCJK**](https://github.com/PianCat/Oh-My-Font-CJK-VF-Extensions) (script, sans-serif, serif) @ _[PianCat](https://t.me/Higanko)_
- Simplified Chinese, Traditional Chinese, Japanese and Korean.
- Nine weights — from Thin to Black.

#### [**SFArabic**](https://gitlab.com/nongthaihoang/oh_my_font/-/raw/master/extensions/sfarabic.zip) (script) @ [_Ahmed Tohamy_](https://t.me/ahmed_tohamy)
> A contemporary interpretation of the Naskh style with a rational and flexible design, this extension of San Francisco is the Arabic system font on Apple platforms. Like San Francisco, SF Arabic features nine weights and variable optical sizes.

#### [**ArabicUI**](https://drive.google.com/file/d/1AtUdsx-VHf20JzNtxXuKiQbkGCaQtBgh/view?usp=drivesdk) (script) @ [_Djamel Eddine Draidi_](https://t.me/DrDraidi)
- Nine weights — from Thin to Black.

#### [**FiraCode**](https://gitlab.com/nongthaihoang/oh_my_font/-/raw/master/extensions/firacode.zip) (monospace) @ [_Source_](https://github.com/tonsky/FiraCode)
- Five weights — from Light to Bold.

#### [**JetBrainsMono**](https://gitlab.com/nongthaihoang/oh_my_font/-/raw/master/extensions/jetbrains.zip) (monospace) @ [_Source_](https://www.jetbrains.com/lp/mono/)
- Eight weights — from Thin to ExtraBold — in both uprights and italics.

#### [**Literata**](https://gitlab.com/nongthaihoang/oh_my_font/-/raw/master/extensions/literata.zip) (serif) @ [_Source_](https://fonts.google.com/specimen/Literata)
- Over one hundred languages using Latin, Greek, and Cyrillic scripts.
- Eight weights — from ExtraLight to Black — in both uprights and italics.
- A contemporary serif typeface family for long-form reading.
- It was commissioned for Google Play Books.

#### [**NerdFontMono**](https://gitlab.com/nongthaihoang/oh_my_font/-/raw/master/extensions/nerdfont.zip) (monospace) @ [_Source_](https://github.com/ryanoasis/nerd-fonts)
- Add Nerd Font icons.

#### [**KillGMSFont**](https://raw.githubusercontent.com/MrCarb0n/killgmsfont/master/extension/killgmsfont.zip) @ [_MrCarb0n_](https://github.com/MrCarb0n/killgmsfont)
> kill GMS's Font Provider service to allow GApps and other apps to use system or custom installed font.

#### [**NotoEmojiPlus**](https://gitlab.com/MrCarb0n/NotoEmojiPlus_OMF/-/raw/master/notoemojiplus.zip?inline=false) (emoji) @ [_Source_](https://gitlab.com/MrCarb0n/NotoEmojiPlus_OMF)
> An emoji extension, it replace system emoji and symbol font with latest noto emoji font based on pre-released unicode emojis and most popular symbols,
> also tries to replace 3rd party apps like Facebook, Google's pre-built emojis.

#### [**AFDKO**](https://gitlab.com/nongthaihoang/oh_my_font/-/raw/master/extensions/afdko.zip) @ [_Source_](https://github.com/adobe-type-tools/afdko)
- Adobe Font Development Kit for OpenType.
- Credit [Termux](https://termux.com/).

## Useful Links
- [Introducing variable fonts – Fonts Knowledge - Google Fonts](https://fonts.google.com/knowledge/introducing_type/introducing_variable_fonts)

## Buy me a coffee ☕
- [PayPal](https://paypal.me/nongthaihoang)
